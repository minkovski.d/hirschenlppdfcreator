const serverless = require('serverless-http')
const phantom = require('phantom')
const express = require('express')
const app = express()
const args = ["./hirschen.js"];
const phantomExecutable = 'phantomjs';


async function createPDF(hash){
  console.log('createPDF');

  const instance = await phantom.create();
  const page = await instance.createPage();
  await page.on("onResourceRequested", function(requestData) {
    console.info('Requesting', requestData.url)
  });

  const status = await page.open('https://stackoverflow.com/');
  console.log(status);

  const content = await page.property('content');
  console.log(content);

  await page.render(hash+'.png');


  await instance.exit();
}

function Uint8ArrToString(myUint8Arr){
  return String.fromCharCode.apply(null, myUint8Arr);
};

app.get('/pdf?', (req, res) => {
  //  res.json({ message: 'PDF Route ' + req.params });
  const hash = 'hirschen';
  createPDF(hash);

  res.json({ message: hash+'.png'});

})

app.get('/:slug?', (req, res) => {
  res.json({ message: 'Your dynamic slug is ' + req.params.slug });
})

createPDF('david');


module.exports.handler = serverless(app);
